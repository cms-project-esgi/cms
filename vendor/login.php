<?php

use Facebook\Facebook;

session_start();
require('.vendor/autoload.php');

$fb = new Facebook\Facebook([
    'app_id' => '270564777724919',
    'app_secret' => '192825a3846a3179ae8e00f843d49bfa',
    'default_graph_version' => 'v2.10',
    ]);
  
  $helper = $fb->getRedirectLoginHelper();
  
  $permissions = ['email']; // Optional permissions
  $loginUrl = $helper->getLoginUrl('./fb-callback.php', $permissions);
  
  echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
