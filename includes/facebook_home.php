<?php

use Facebook\Facebook;

require('/var/www/html/wp-content/plugins/cms/vendor/autoload.php');

$fb = new Facebook([
    'app_id' => '270564777724919',
    'app_secret' => '192825a3846a3179ae8e00f843d49bfa',
    'default_graph_version' => 'v2.10',
]);

$helper = $fb->getRedirectLoginHelper();
if (isset($_GET['state'])) {
    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
}

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('http://localhost:8080/wp-admin/admin.php?page=cms%2Fincludes%2Ffbcallback.php', $permissions);

echo '<a href="' . $loginUrl . '">Se connecter avec Facebook</a>';
