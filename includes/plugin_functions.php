<?php

add_action('admin_menu', 'facebook_menu');

function facebook_menu() {
    add_menu_page(
        'Facebook Menu',
        'Facebook',
        'manage_options',
        'cms/includes/facebook_home.php'
    );
    add_submenu_page(
        "cms/includes/facebook_home.php",
        "Facebook Access Token",
        "Access Token",
        "manage_options",
        "cms/includes/fbcallback.php"
    );
}

/* Widget */

class Plugin_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct(
			'plugin_widget', 
			'Widget ShowComments', 
			array(
				'description' => __('Widget montrant les commentaires du post', 'mypluginlg')
			)
		);
	}

	public function widget( $args, $instance ) {
        
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $before_widget;
        if (!empty($title))
        	echo $before_title . $title . $after_title;
        echo $after_widget;


        $users = get_users();
        //var_dump($users);

		foreach ( $users as $user ) {
		    $user_id = $user->ID;
		    $post_id = get_the_ID();
		    $args = array(
		    'user_id' => $user_id,
		    'post_id' => $post_id,
			);
			$comments = get_comments( $args );
			 
			foreach ( $comments as $comment ) {
				//ar_dump($comment);
			    echo "Commentaire : " . $comment->comment_content;
			    echo ("<br>");

			    echo "A été posté par : " . $comment->comment_author ;
			    echo ("<br>");
			    echo ("<br>");
			}
		}
    }	

    public function form( $instance ) {
        // outputs the options form in the admin
        $title= isset($instance['title']) ? $instance['title'] : '';
        echo'<label for="'.$this->get_field_name('title').'">Title : </label>
        <input id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" type="text" value="'.$title.'">';
    }
 
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags( $new_instance['title']) : '';
        return $instance;
    }

}

add_action('widgets_init', 'myplugin_PluginWidget');

function myplugin_PluginWidget() {
	register_widget('Plugin_Widget');
}


/* Shortcodes */

add_shortcode('image', 'imageShortcode');

function imageShortcode($atts) {

	$image = shortcode_atts(array(
		'src' => $atts,
	), $atts); 

	return "<img src='". $image['src'] ."'></img>";

}

add_action( 'admin_init', 'fb_privacy_policy' );

function fb_privacy_policy() {
    if ( ! function_exists( 'wp_add_privacy_policy_content' ) ) {
        return;
    }
    $content = '<p class="privacy-policy-tutorial">' . __( 'Ci dessous, notre politique concernant la récupération des données lié a Facebook', 'my_plugin_textdomain' ) . '</p>'
        . '<strong class="privacy-policy-tutorial">' . __( 'Politique de confidentialité :', 'my_plugin_textdomain' ) . '</strong> '
        . sprintf(
            __( 'A la connexion avec facebook...', 'my_plugin_textdomain' ),
            'https://example.com/privacy-policy'
        );
    wp_add_privacy_policy_content( 'Facebook', wp_kses_post( wpautop( $content, false ) ) );
}
